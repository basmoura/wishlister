require 'spec_helper'

describe User do
  context 'is invalid without' do
    it 'name' do
      user = FactoryGirl.build(:user, name: nil)
      expect(user).to_not be_valid
    end

    it 'token' do
      user = FactoryGirl.build(:user, token: nil)
      expect(user).to_not be_valid
    end
  end

  it 'is valid' do
    user = FactoryGirl.build(:user)
    expect(user).to be_valid
  end
end

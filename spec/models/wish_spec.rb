require 'spec_helper'

describe Wish do
  context 'is invalid without a' do
    it 'venue_name' do
      wish = FactoryGirl.build(:wish, venue_name: nil)
      expect(wish).to_not be_valid
    end

    it 'venue_photo' do
      wish = FactoryGirl.build(:wish, venue_photo: nil)
      expect(wish).to_not be_valid
    end

    it 'user' do
      wish = FactoryGirl.build(:wish, user_id: nil)
      expect(wish).to_not be_valid
    end
  end
end

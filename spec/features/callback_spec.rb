require 'spec_helper'

feature 'Login with foursquare' do
  scenario 'authenticate with facebook and redirect_to /session/callback' do
    visit "/"
    click_link('login_foursquare')
    page.find('span', text: 'Login with Facebook').click
    within_window(page.driver.browser.window_handles.last) do
      fill_in('email', with: 'fb_user')
      fill_in('pass', with: 'fb_password')
      click_on('Log In')
    end
    expect(page).to have_content('Autenticado com sucesso')
  end
end

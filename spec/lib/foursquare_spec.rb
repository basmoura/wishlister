require 'spec_helper'
require 'foursquare'

describe Foursquare do
  context 'get response' do
    url = "https://api.foursquare.com/v2/users/self?oauth_token=ZOP4B3DUSKJBA0GWB31Y3OWAAA3XQ4EP0FP1I02YWQE2CW10&v=2014060"

    it 'returns a string' do
      res = Foursquare.get_response(url)
      expect(res).to be_kind_of(String)
    end

    it 'returns status 200' do
      uri = URI(url)
      res = Net::HTTP.get_response(uri)
      expect(res.code).to eq('200')
    end
  end
end

require 'spec_helper'

describe WishesController do
  describe 'POST create' do
    before :each do
      @user = FactoryGirl.create(:user)
    end

    context 'with valid attributes' do
      it 'saves the new wish in the database' do
        expect { post :create, user_id: @user, wish: attributes_for(:wish, user_id: @user.id) }.to change(Wish, :count).by(1)
      end

      it 'redirects to user_wishes_path' do
        post :create, user_id: @user, wish: attributes_for(:wish, user_id: @user.id)
        expect(response).to redirect_to(user_wishes_path)
      end
    end
  end

  describe 'DELETE destroy' do
    before :each do
      @user = FactoryGirl.create(:user)
      @wish = FactoryGirl.create(:wish, user: @user)
    end

    it 'delete the wish' do
      expect { delete :destroy, id: @wish, user_id: @user }.to change(Wish, :count).by(-1)
    end

    it 'redirects to user_wishes' do
      delete :destroy, id: @wish, user_id: @user
      expect(response).to redirect_to(user_wishes_path)
    end
  end
end

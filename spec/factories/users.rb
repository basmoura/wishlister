FactoryGirl.define do
  factory :user do
    name      'Breno'
    last_name 'Moura'
    photo     'http://photos.whishlister.com/basmoura'
    token     'ZOP4B3DUSKJBA0GWB31Y3OWAAA3XQ4EP0FP1I02YWQE2CW10'
  end
end

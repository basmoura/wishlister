# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :wish do
    venue_name "JayaApps"
    venue_photo "http://jaya-apps.com/images/company.jpg"
    association :user
  end
end

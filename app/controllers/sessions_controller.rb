require 'foursquare'

class SessionsController < ApplicationController
  def callback
    callback_url = Foursquare.get_response("https://foursquare.com/oauth2/access_token?client_id=#{Rails.application.secrets['client_id']}&client_secret=#{Rails.application.secrets['client_secret']}&grant_type=authorization_code&redirect_uri=http://localhost:3000/sessions/callback&code=#{params[:code]}")
    @token = Foursquare.get_token(callback_url)
    api_response = Foursquare.json_parse("https://api.foursquare.com/v2/users/self?oauth_token=#{@token}&v=20140525")

    authenticate_or_authorize_user(api_response)
  end

  def destroy
    session[:user_id] = nil
    Rails.cache.delete("checkins")
    redirect_to root_path
    flash[:success] = t("session.signout")
  end

  private
  def authenticate_or_authorize_user(resp)
    user = User.where(token: @token).take

    if user
      sign_in(user)
    else
      sign_up(resp, @token)
    end
  end

  def sign_in(user)
    session[:user_id] = user.id
    current_user
    redirect_to user_wishes_path(user.id)
    flash[:success] = t("session.signin")
  end

  def sign_up(resp, token)
    user = User.new
    user.name = resp["response"]["user"]["firstName"]
    user.last_name =  resp["response"]["user"]["lastName"]
    user.token = token
    user.photo = "#{resp["response"]["user"]["photo"]["prefix"]}300x300#{resp["response"]["user"]["photo"]["suffix"]}"
    user.save

    sign_in user
  end
end

require 'foursquare'

class WishesController < ApplicationController
  def index
    @checkins ||= Foursquare.checkins(current_user.token)
    @wishes = Wish.where(user_id: current_user.id)
  end

  def create
    @wish = Wish.new(wish_params)
    if @wish.save
      redirect_to user_wishes_path
      flash[:success] = t("wish.add")
    else
      redirect_to user_wishes_path
      flash[:warning] = t("wish.exists")
    end
  end

  def destroy
    @wish = Wish.find(params[:id])
    @wish.destroy
    redirect_to user_wishes_path
    flash[:success] = t("wish.del")
  end

  private
  def wish_params
    params.require(:wish).permit(:venue_name, :venue_photo, :user_id)
  end
end

class Wish < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :venue_name, :venue_photo, :user_id
  validates_uniqueness_of :venue_name
end

Rails.application.routes.draw do
  root "home#index"

  resources :users do
    resources :wishes
  end

  resource :session, only: [:destroy]
  get '/sessions/callback', to: "sessions#callback"
end
